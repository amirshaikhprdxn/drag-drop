var priority = document.querySelectorAll('.priority');
var task = document.querySelectorAll('.task');
var start = document.getElementById('start');
var restart = document.getElementById('restart');
var target, srcId, dropable;
var flag = 0;

start.addEventListener('click',function(event){
	event.preventDefault();
	console.log(flag)
	if (flag==0) {
		event.target.innerHTML = 'stop';
		flag = 1;
		Array.from(priority).forEach(function (e){
			e.addEventListener('dragover',function(event){
				event.preventDefault();
			});
		});
		
		Array.from(priority).forEach(function (e){
			e.addEventListener('drop',function(event){
				event.preventDefault();
				target = event.target;
				srcId = event.dataTransfer.getData('srcId');
				dropable = target.classList.contains('priority');
				if(dropable){
					target.appendChild(document.getElementById(srcId));
				}
			});
		});
		
		Array.from(task).forEach(function (e){
			e.setAttribute('draggable', true);
			e.addEventListener('dragstart',function(event){
				event.dataTransfer.setData("srcId", event.target.id);
			});
		});
	} else {
		flag = 0;
		event.target.innerHTML = 'start';
		Array.from(task).forEach(function (e){
			e.setAttribute('draggable', false);
		});
	}
});

restart.addEventListener('click',function(event){
	event.preventDefault();
	location.reload();
});